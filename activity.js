// *** Aggregation in MongoDB and Query Case Studies ***

// solution #2: count onSale fruits

db.fruits.aggregate([
		{ $match: {onSale: true} },
		{ $count: "saleCount" }			// 3
	])

// solution #3: count stock more than 20

db.fruits.aggregate([
		{ $match: {stock: {$gt: 20}} },
		{ $count: "stockCount" }		// 1
	])

// solution #4: average price of fruits onSale

db.fruits.aggregate([
		{ $match: {onSale: true} },
		{ $group: {
			_id: "null",
			avePrice: {$avg: "$price"}	// 36.666667
		}
		}			
	])


// solution #5: highest price of fruit per supplier

db.fruits.aggregate([
		{ $match: {onSale: true} },
		{ $group: {
			_id: "null",
			maxPrice: {$max: "$price"}	// 50
		}
		}			
	])

// solution #6: lowest price of fruit per supplier

db.fruits.aggregate([
		{ $match: {onSale: true} },
		{ $group: {
			_id: "null",
			minPrice: {$min: "$price"}	// 20
		}
		}			
	])






